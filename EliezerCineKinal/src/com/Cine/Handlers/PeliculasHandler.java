package com.Cine.Handlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.Cine.Peliculas.Pelicula;
import com.Cine.Datos.DataBase;

public class PeliculasHandler {

	private static final PeliculasHandler INSTANCE = new PeliculasHandler();
	
	public static final PeliculasHandler getInstance(){
		return INSTANCE;
	}
	
	public Pelicula getPeliculaById(Integer id){
		Pelicula pl = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula WHERE IDPELICULA = " + id);
		
		try {
			while(result.next()){
				pl = new Pelicula(result.getInt("idPelicula"), result.getString("nombrePelicula"), result.getString("descripcionPelicula"), result.getString("trailerPelicula"),  result.getString("imagen") ,result.getInt("duracionPelicula"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pl;
	}
	
	public ArrayList<Pelicula> getPeliculaList(){
		
		ArrayList<Pelicula> lista = new ArrayList<Pelicula>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula");
		
		try {
			while(result.next()){
				lista.add(new Pelicula(result.getInt("idPelicula"), result.getString("nombrePelicula"), result.getString("descripcionPelicula"), result.getString("trailerPelicula"),  result.getString("imagen") ,result.getInt("duracionPelicula")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lista;
	}
	
	
}
