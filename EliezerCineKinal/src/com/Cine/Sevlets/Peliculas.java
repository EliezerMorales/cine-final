package com.Cine.Sevlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Cine.Handlers.PeliculasHandler;

/**
 * Servlet implementation class Peliculas
 */
@WebServlet("/Peliculas.do")
public class Peliculas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Peliculas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
if (request.getParameter("idPelicula") != null){
			
			Integer pelicula = Integer.parseInt(request.getParameter("idPelicula"));
			
			request.setAttribute("Peliculas", PeliculasHandler.getInstance().getPeliculaById(pelicula));			
			request.getRequestDispatcher("Peliculas.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
