package com.Cine.Peliculas;

public class Pelicula {
	private Integer idPelicula;
	private String nombrePelicula;
	private String descripcionPelicula;
	private String trailePelicula;
	private String imagen;
	private Integer duracion;
	public Integer getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getNombrePelicula() {
		return nombrePelicula;
	}
	public void setNombrePelicula(String nombrePelicula) {
		this.nombrePelicula = nombrePelicula;
	}
	public String getDescripcionPelicula() {
		return descripcionPelicula;
	}
	public void setDescripcionPelicula(String descripcionPelicula) {
		this.descripcionPelicula = descripcionPelicula;
	}
	public String getTrailePelicula() {
		return trailePelicula;
	}
	public void setTrailePelicula(String trailePelicula) {
		this.trailePelicula = trailePelicula;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public Integer getDuracion() {
		return duracion;
	}
	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}
	public Pelicula(Integer idPelicula, String nombrePelicula,
			String descripcionPelicula, String trailePelicula, String imagen,
			Integer duracion) {
		super();
		this.idPelicula = idPelicula;
		this.nombrePelicula = nombrePelicula;
		this.descripcionPelicula = descripcionPelicula;
		this.trailePelicula = trailePelicula;
		this.imagen = imagen;
		this.duracion = duracion;
	}
	public Pelicula() {
		super();
		// TODO Auto-generated constructor stub
	}

		
}
