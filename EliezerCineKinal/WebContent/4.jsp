<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HOBBIT</title>
	<link rel="stylesheet" type="text/css" href="CSS/4.css">
</head>
<body>
	<h1>HOBBIT</h1>
	<img src="Imagenes/4.jpg"/>
	<iframe width="400" height="300" src="http://www.youtube.com/embed/iVAgTiBrrDA" frameborder="0" allowfullscreen></iframe>
	<p><strong>Sinopsis:</strong> El director Peter Jackson culmina el ciclo de El hobbit con esta tercera parte que se centrará en el final de las aventuras del pequeño Bilbo Bolson y su regreso a Bolsón Cerrado, su hogar. Después de enfrentarse con el dragón Smaug y de obtener el famoso anillo de poder de las garras del siniestro Gollum, Thorin sacrifica su amistad y honor para protegerlo. Incapaz de ayudar a su amigo a entrar en razón, Bilbo se ve obligado a tomar una peligrosa decisión sin conocer aún los obstáculos que le esperan por delante. Sauron, “el señor oscuro”, ha vuelto a la Tierra Media junto a legiones de orcos para atacar sigilosamente la Montaña Solitaria. A medida que la oscuridad crece, los enanos, elfos y hombres deben decidir entre juntarse para derrotar a un enemigo común o ser destruidos. Bilbo se encuentra luchando por su propia vida y la de sus compañeros.</p>
	<input type="button" value="Comprar." onclick="window.location.href='Formulario.html'">
</body>
</html>