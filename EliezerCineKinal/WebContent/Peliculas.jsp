<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="Carousel/style.css">
  <link rel="stylesheet" type="text/css" href="Carousel/jcarousel.basic.css">

  <script type="text/javascript" src="Carousel/jquery.js"></script>
  <script type="text/javascript" src="Carousel/jquery.jcarousel.min.js"></script>

  <script type="text/javascript" src="Carousel/jcarousel.basic.js"></script>
  <title>ELIEZER MORALES</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index.jsp">Proyecto<span class="logo_colour">CINE</span></a></h1>
          <h2>ELIEZER ADELSO MORALES ALVAREZ</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="PeliculasServlet.do">Home</a></li>
          <li><a href="PeliculasServlet.do">Cartelera</a></li>
          <li><a href="PeliculasServlet.do">Estrenos</a></li>
          <li><a href="PeliculasServlet.do">asdf</a></li>
          <li><a href="PeliculasServlet.do">Contactenos</a></li>
        </ul>
      </div>
    </div>
    
    <div id="content_header"></div>
    <div id="site_content">
    <div class="wrapper">
        <hr>
        <hr>
        <hr>
        
        <hr>
        <hr><hr>
        <hr>
        <hr>
        
        <hr>
        <hr>
        <h1>Bienvenido a Cine KINAL</h1>
        <p>Este es un proyecto de sexto grado pensado para el desarrollo y utilidad de JEE(JAVA ENTERPRISE EDITION)</p>
        <p>Ademas posee bases de HTML, CSS, JQuery, JAVAScript.</p>
        <hr>
        <hr>
        <hr>
        
        <hr>
        <hr><hr>
        <hr>
        <hr>
        
        <hr>
        <hr>
		<div id="content">
    	   	<h1>${Peliculas.getNombrePelicula()}</h1>
    	   	
    	   	<p>${Peliculas.getDescripcionPelicula()}</p>
			<img src="${Peliculas.getImagen()}">
			<iframe src="${Peliculas.getTrailePelicula()}" width="700px" height="500px"></iframe>
		</div>
        <hr>
        <hr>
        <hr>
        
        <hr>
        <hr>
        <hr>
    </div>
        
    </div>
    
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index.html">Home</a> | <a href="examples.html">Otros</a> | <a href="contacto.jsp">Contactenos</a></p>
      <p>Todos los Derechos Reservador &copy; Eliezer Morales | <a href="http://validator.w3.org/check?uri=referer">HTML5</a> | <a href="http://www.google.com">Google</a> | <a href="http://www.bing.com">bing</a></p>
    </div>
  </div>
  
</body>
</html>