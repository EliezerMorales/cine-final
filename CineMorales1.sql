CREATE TABLE Categoria(
  idCategoria INT not null,
  nombreCategoria VARCHAR(10) not null,
  descriptionCategoria VARCHAR(255) not null,
  PRIMARY KEY(idCategoria)
) 

CREATE SEQUENCE categ_seq;

CREATE OR REPLACE TRIGGER Categoria_bir 
BEFORE INSERT ON Categoria
FOR EACH ROW

BEGIN
  SELECT categ_seq.NEXTVAL
  INTO   :new.idCategoria
  FROM   dual;
END;

CREATE TABLE Clasificacion(
  idClasificacion INT not null,
  nombreClasificacion VARCHAR(20),
  descripcionClasificacion VARCHAR(255),
  PRIMARY KEY(idClasificacion)
)


CREATE SEQUENCE clasif_seq;

CREATE OR REPLACE TRIGGER Clasificacion_bir 
BEFORE INSERT ON Clasificacion
FOR EACH ROW

BEGIN
  SELECT clasif_seq.NEXTVAL
  INTO   :new.idClasificacion
  FROM   dual;
END;

CREATE TABLE Funcion(
  idFuncion INT not null,
  nombreFuncion VARCHAR(25) not null,
  descripcionFuncion VARCHAR(255) not null,
  presioFuncion VARCHAR(255) not null,
  PRIMARY KEY(idFuncion)
)

CREATE SEQUENCE funcion_seq;

CREATE OR REPLACE TRIGGER Funcion_bir 
BEFORE INSERT ON Funcion
FOR EACH ROW

BEGIN
  SELECT funcion_seq.NEXTVAL
  INTO   :new.idFuncion
  FROM   dual;
END;

CREATE TABLE Venta(
  idVenta INT not null,
  nombrePersona VARCHAR(25) not null,
  numeroTransaccion INT not null,
  correoPersona VARCHAR(25) not null,
  telefonoPersona int not null,
  tipoPago VARCHAR(25) not null,
  PRIMARY KEY(idVenta)
)

CREATE SEQUENCE venta_seq;

CREATE OR REPLACE TRIGGER Venta_bir 
BEFORE INSERT ON Venta
FOR EACH ROW

BEGIN
  SELECT venta_seq.NEXTVAL
  INTO   :new.idVenta
  FROM   dual;
END;

CREATE TABLE Complejo(
  idComplejo INT not null,
  nombreComplejo VARCHAR(25) not null,
  direccionComplejo VARCHAR(25) not null,
  callCenter INT not null,
  latitudComplejo NUMBER(6,2),
  PRIMARY KEY(idComplejo)
)

CREATE SEQUENCE Complejo_seq;

CREATE OR REPLACE TRIGGER complejo_bir 
BEFORE INSERT ON Complejo
FOR EACH ROW

BEGIN
  SELECT Complejo_seq.NEXTVAL
  INTO   :new.idComplejo
  FROM   dual;
END;

CREATE TABLE Sala(
  idSala INT not null,
  idComplejo INT not null,
  nombreSala VARCHAR(25) not null,
  tipoSala VARCHAR(10) not null,
  PRIMARY KEY(idSala),
  CONSTRAINT Sala_fk FOREIGN KEY(idComplejo) REFERENCES Complejo(idComplejo)
)

CREATE SEQUENCE Sala_seq;

CREATE OR REPLACE TRIGGER sala_bir 
BEFORE INSERT ON Sala
FOR EACH ROW

BEGIN
  SELECT Sala_seq.NEXTVAL
  INTO   :new.idSala
  FROM   dual;
END;


CREATE TABLE Asiento(
  idAsiento INT not null,
  idSala INT not null,
  nombreAsiento VARCHAR(10) not null,
  PRIMARY KEY(idAsiento),
  CONSTRAINT Asiento_fk FOREIGN KEY(idSala) REFERENCES Sala(idSala)
)

CREATE SEQUENCE Asiento_seq;

CREATE OR REPLACE TRIGGER Asiento_bir 
BEFORE INSERT ON Asiento
FOR EACH ROW

BEGIN
  SELECT Asiento_seq.NEXTVAL
  INTO   :new.idAsiento
  FROM   dual;
END;

CREATE TABLE Pelicula(
  idPelicula INT not null,
  nombrePelicula VARCHAR(50) not null,
  descripcionPelicula VARCHAR(255) not null,
  trailerPelicula VARCHAR(255) not null,
  duracionPelicula NUMBER(6,2) not null,
  PRIMARY KEY(idPelicula)
)

CREATE SEQUENCE Pelicula_seq;

CREATE OR REPLACE TRIGGER Pelicula_bir 
BEFORE INSERT ON Pelicula
FOR EACH ROW

BEGIN
  SELECT Pelicula_seq.NEXTVAL
  INTO   :new.idPelicula
  FROM   dual;
END;

CREATE TABLE Cartelera(
  idCartelera INT not null,
  idSala INT not null,
  idPelicula INT not null,
  horaInicio NUMBER(6,2) not null,
  horaFin NUMBER(6,2) not null,
  PRIMARY KEY(idCartelera),
  CONSTRAINT PeliculaSala_fk FOREIGN KEY(idSala) REFERENCES Sala(idSala),
  CONSTRAINT CarteleraPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula)
)

CREATE SEQUENCE cartelera_seq;

CREATE OR REPLACE TRIGGER Cartelera_bir 
BEFORE INSERT ON Cartelera
FOR EACH ROW

BEGIN
  SELECT cartelera_seq.NEXTVAL
  INTO   :new.idCartelera
  FROM   dual;
END;

CREATE TABLE PeliculaCategoria(
  idPelicula INT not null,
  idCategoria INT NOT NULL,
  CONSTRAINT CategoriaPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula),
  CONSTRAINT PeliculaCateria_fk FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
)

CREATE TABLE VentaDetalle(
  idVenta INT not null,
  idAsiento INT not null,
  idCartelera INT not null,
  idFuncion INT not null,
  CONSTRAINT VentaDetalleVenta_fk FOREIGN KEY(idVenta) REFERENCES Venta(idVenta),
  CONSTRAINT VentaDetalleAsiento_fk FOREIGN KEY(idAsiento) REFERENCES Asiento(idAsiento),
  CONSTRAINT VentaCartelera_fk FOREIGN KEY(idCartelera) REFERENCES Cartelera(idCartelera),
  CONSTRAINT VentaFuncion_fk FOREIGN KEY(idFuncion) REFERENCES Funcion(idFuncion)
)

CREATE TABLE EstrenoPelicula(
  idEstreno INT not null,
  idPelicula INT not null,
  fechaPelicula DATE not null,
  PRIMARY KEY(idEstreno),
  CONSTRAINT EstrenoPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula)
)

CREATE SEQUENCE Estreno_seq;

CREATE OR REPLACE TRIGGER Estreno_bir 
BEFORE INSERT ON EstrenoPelicula
FOR EACH ROW

BEGIN
  SELECT Estreno_seq.NEXTVAL
  INTO   :new.idEstreno
  FROM   dual;
END;

Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'Annabelle','La mu�eca fue fabricada por una empresa juguetera infantil que comercializaba los productos de Raggedy Ann, siendo esta una de las unidades.','http://www.youtube.com/embed/paFgQNPGlsg',1.5,'Imagenes/2.jpg');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'Busqueda implacable 3','Liam Neeson regresa como el ex-agente especial Bryan Mills. Su reconciliacion se ha visto inesperadamente 
	truncada tras el brutal asesinato de su ex mujer. Consumido por la ira y acusado de la muerte de su ex mujer, Mills se ve obligado a huir 
	de la implacable persecuci�n de la CIA, el FBI y la polic�a. Una vez m�s, deber� usar sus �habilidades especiales� para hacer justicia,  
	dar caza a los verdaderos asesinos y proteger lo �nico que le queda en la vida: su hija.','http://www.youtube.com/embed/r0EPt7-J1eY',1.45,'Imagenes/1.jpg');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'Exodus','Exodus: Dioses y reyes se basa en el libro b�blico de �El �xodo� junto con algunas de las historias del Antiguo Testamento. La pel�cula narra la vida de Mois�s desde el d�a en el que naci� hasta su muerte. De este modo repasa momentos clave como su adopci�n en la familia real egipcia o su desaf�o hacia el fara�n. Aunque principalmente se centra en la haza�a de liberar a 600.000 esclavos hebreos y su viaje por el desierto del Sinai. En el papel de Mois�s encontramos al brit�nico Christian Bale.','http://www.youtube.com/embed/mZPMU9riKl4',1.5,'Imagenes/3.png');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'El Hobbit','El director Peter Jackson culmina el ciclo de El hobbit con esta tercera parte que se centrar� en el final de las aventuras del peque�o Bilbo Bolson y su regreso a Bols�n Cerrado, su hogar. Despu�s de enfrentarse con el drag�n Smaug y de obtener el famoso anillo de poder de las garras del siniestro Gollum, Thorin sacrifica su amistad y honor para protegerlo. Incapaz de ayudar a su amigo a entrar en raz�n, Bilbo se ve obligado a tomar una peligrosa decisi�n sin conocer a�n los obst�culos que le esperan por delante. Sauron, �el se�or oscuro�, ha vuelto a la Tierra Media junto a legiones de orcos para atacar sigilosamente la Monta�a Solitaria. A medida que la oscuridad crece, los enanos, elfos y hombres deben decidir entre juntarse para derrotar a un enemigo com�n o ser destruidos. Bilbo se encuentra luchando por su propia vida y la de sus compa�eros.','http://www.youtube.com/embed/iVAgTiBrrDA',2,'Imagenes/4.jpg');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'Pinguinos de Madagascar','Tras el �xito alcanzado en televisi�n, los personajes m�s queridos por el p�blico infantil, los ping�inos de la pel�cula Madagascar, dan el salto a la gran pantalla protagonizando su propia pel�cula animada: el �spin-off� Los ping�inos de Madagascar. Skipper, Kowalski, Rico, Cabo y el resto del equipo se embarcan en una aventura con otro grupo de esp�as, �Viento Norte�, para luchar contra un malvado pulpo que quiere acabar con todos los ping�inos del mundo.','http://www.youtube.com/embed/FD2wv_-owEM',1.4,'Imagenes/5.jpg');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'Alma Salvaje','Despu�s de a�os de un comportamiento irresponsable, una adicci�n a la hero�na y la destrucci�n de su matrimonio, Strayed toma una decisi�n precipitada. Obsesionada por los recuerdos de su madre, Bobbi (la candidata al Premio de la Academia, Laura Dern), y carente de toda experiencia, emprende �totalmente sola� una caminata de m�s de mil millas por el Sendero de las Cimas del Pac�fico. ALMA SALVAJE revela con enorme fuerza sus terrores y sus placeres a medida que sigue adelante en una odisea que la enloquece, la fortalece y acaba cur�ndola.','http://www.youtube.com/embed/bfMgUvU2GXM',1.3,'Imagenes/6.jpg');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'Quiero matar a mi jefe 2',' Nick, Dale y Kurt, estan cansados de depender de los de arriba y juntos deciden convertirse en sus propios jefes lanzando su propia empresa. Pero un inversor con mucha labia echa por tierra sus planes. Vencidos y desesperados, y adem�s sin recursos legales, los tres empresarios aficionados urden un insensato plan para secuestrar al hijo adulto del inversor y pedir un rescate por �l para volver a conseguir el control de su empresa.','http://www.youtube.com/embed/LQyqUAjJBas',1.28,'Imagenes/7.jpg');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'Grandes Heroes','En la pel�cula �6 H�roes (Big Hero 6)�, Hiro Hamada es un experto en rob�tica que se encuentra dentro de un complot criminal que amenaza con destruir r�pidamente la tecnol�gica ciudad de San Fransokyo. Hiro y su mejor amigo, un robot llamado Baymax, unir�n sus fuerzas a un equipo de novatos luchadores contra el crimen en la misi�n de salvar a su ciudad. Inspirado en los c�mics de Marvel del mismo nombre, y en c�mics de acci�n �Big Hero 6?.','http://www.youtube.com/embed/DrAClyAk4O8',1.4,'Imagenes/8.jpg');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'OUIJA','Un fin de semana lleno de diversi�n se vuelve mortal cuando un grupo de j�venes quedan atrapados en el teatro despu�s de que un esp�ritu sea liberado por jugar a la ouija.','http://www.youtube.com/embed/_T1Jj1inE8M',1.29,'Imagenes/9.jpg');
Insert into PELICULA (IDPELICULA,NOMBREPELICULA,DESCRIPCIONPELICULA,TRAILERPELICULA,DURACIONPELICULA,IMAGEN) values (null,'Dracula','Dr�cula, la historia jam�s contada es una aventura �pica de acci�n protagonizada por Luke Evans (R�pidos y Furiosos 6, El Hobbit), que cuenta la historia del Conde Vlad Tepes, quien para defender a su pueblo de la invasi�n del Sult�n Mehmed (Dominic Cooper) acude con un oscuro ser, quien le promete hacerlo invencible en la batalla a cambio de un alto precio; dando as� origen a la leyenda del Conde Dr�cula.','http://www.youtube.com/embed/fp9cNx99IKY',1.5,'Imagenes/10.jpg');
